# Prepare is used for Seasnet Lab enviornment setup
compile:
	javac Main.java
run:
	java Main
clean:
	rm *.class && rm DNAInversion/*.class
prepare:
	setenv PATH /usr/local/cs/bin:${PATH}
