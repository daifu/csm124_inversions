import java.util.*;
import java.io.*;

class DNATest {
    public void run() {
		int start = 0;
        BufferedReader br = null;
        char[] cbuf = new char[10];
		char[] step_buf = new char[1];

        try {
            br = new BufferedReader(new FileReader("DNAInversion/reference_genome.txt"));
            br.skip(start);
			int count = 0;
			while (br.read(step_buf, 0, 1) > 0) {
				cbuf[count] = step_buf[0];
				count++;
				if (count == 10) {
					break;
				}
			}
			String key = String.valueOf(cbuf);
            do {
				System.out.println(key +" : "+start);
				key = key.substring(1);
				key += String.valueOf(step_buf);
				start+=1;
            } while (br.read(step_buf, 0, 1) > 0);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
	public static void main(String[] args) {
		DNATest t = new DNATest();
		t.run();
	}
}