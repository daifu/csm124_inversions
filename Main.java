import DNAInversion.*;
import java.util.*;

public class Main {
	public static void main(String [] args) {
		try	{
			// Create human genome:
			Input.generate_reference_genome();
			ArrayList<Integer>revList = Input.generate_human_genome();
			System.out.println("Inversed List:");
			System.out.println(revList);

			// Get input
			int coverage = Input.get_user_input_coverage();
			int coverage_range = coverage * Input.max / Input.readLength;
			String algorithm = Input.get_user_input_algorithm();

			// Start the program and time it
			long startTime = System.currentTimeMillis();
			Mapper map = new Mapper();

			// Build the most expensive table:
			if (algorithm.equals("2")) {
				map.build_hash_table();
			} else if (algorithm.equals("3")) {
				map.parallel_build_hash_table(4); // 4 threads
			}

			// Based line method does not need a mapper
			TreeSet<PairEndReads> pairList = new TreeSet<PairEndReads>();
			int min_gap = Math.max(600, Input.revLen);
			for (int i = 0; i < coverage_range; i++) {
				PairEndReads reads = Input.get_pair_end_reads();
				PairEndReads result;
				if (algorithm.equals("1")) {
					result = map.based_line_search(reads);
				} else {
					result = map.hashtable_based_line_search(reads);
				}
				if ((result.isLeftReversed || result.isRightReversed) && result.get_gap() > 0) {
					pairList.add(result);
				} else if (result.get_gap() > min_gap) {
					// assume the gap will not more than 10 if it is not reversed
					pairList.add(result);
				}
			}

			// summarize the potential pairList
			InversionSignature inverSign = new InversionSignature(pairList, revList);
			inverSign.check_inv_signature();
			inverSign.print_stat();

			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println("Total time: "+totalTime+" milliseconds");

			// inverSign.debug();
		} catch (DNAInversionException e) {
			System.out.println(e);
			System.exit(1);
		}
	}
}