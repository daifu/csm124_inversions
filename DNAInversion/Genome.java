package DNAInversion;

public class Genome {
	public String genome_str;

	public Genome(String str) {
		genome_str = str;
	}

	public void set(String str) {
		genome_str = str;
	}

	public String get(String str) {
		return genome_str;
	}
}