package DNAInversion;

import java.util.*;
import java.io.*;

public class BuildThreads extends Thread {
    private int start;
    private int end;
    Hashtable<String, ArrayList<Integer>> hashtable = null;
    private int run_gap;

    static void threadMessage(String message) {
        String threadName =
            Thread.currentThread().getName();
        System.out.format("%s: %s%n",
                          threadName,
                          message);
    }

    public BuildThreads(Hashtable<String, ArrayList<Integer>> hashtable, int start, int end, int run_gap) {
        this.hashtable = hashtable;
        this.start = start;
        this.end = end;
        this.run_gap = run_gap;
    }

    public void run() {
        BufferedReader br = null;
        char[] cbuf = new char[Mapper.key_length];
        char[] step_buf = new char[1];

        try {
            br = new BufferedReader(new FileReader(Input.get_reference_file()));
            br.skip(start);

            int count = 0;
            while (br.read(step_buf, 0, 1) > 0) {
                cbuf[count] = step_buf[0];
                count++;
                if (count == 10) {
                    break;
                }
            }
            String key = String.valueOf(cbuf);
            do {
                if (hashtable.containsKey(key)) {
                    add(key, start);
                } else {
                    create(key, start);
                }
                key = key.substring(1);
                key += String.valueOf(step_buf);
                start += 1;
            } while ((br.read(step_buf, 0, 1) > 0) && (start <= end));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public synchronized void create(String key, int start) {
        ArrayList<Integer> tmp;
        tmp = new ArrayList<Integer>();
        tmp.add(start);
        hashtable.put(key, tmp);
    }

    public synchronized void add(String key, int start) {
        ArrayList<Integer> tmp = hashtable.get(key);
        if (tmp.indexOf(start) < 0) {
            tmp.add(start);
            hashtable.put(key, tmp);
        }
    }
}

