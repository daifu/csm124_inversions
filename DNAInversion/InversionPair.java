package DNAInversion;

import java.util.*;

public class InversionPair {
	public int posLeft;
	public int posRight;

	public InversionPair() {}

	public InversionPair(int left, int right) {
		posLeft = left;
		posRight = right;
	}
}