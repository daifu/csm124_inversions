package DNAInversion;

import java.util.*;

public class InversionSignature {
	public TreeSet<PairEndReads> pairList;
	public ArrayList<InversionPair> inversionPairs;
	public int[] revList;

	public InversionSignature(TreeSet<PairEndReads> list, ArrayList<Integer> inRevList) {
		pairList = list;
		revList = Input.convertIntegers(inRevList);
		Arrays.sort(revList);
		inversionPairs = new ArrayList<InversionPair>();
	}

	public synchronized void check_inv_signature() {
		int counter = 0;
		// make sure the first is > 0
		Iterator<PairEndReads> itr = pairList.iterator();
		PairEndReads p = null;
		int[] pos;
		ArrayList<InversionPair> invTemps = new ArrayList<InversionPair>();

		while(itr.hasNext()) {
			pos = check_inv_signature_helper(itr);

			if (pos[0] > 0 && pos[1] > 0 && (pos[1] > pos[0])) {
				InversionPair iPair = new InversionPair(pos[0], pos[1]);
				invTemps.add(iPair);
				counter++;
			}
		}

		inversionPairs = invTemps;

	}

	public synchronized int[] check_inv_signature_helper(Iterator<PairEndReads> itr) {
		// if findDir is false, means left
		// else means right
		boolean found = false;
		int[] pos = new int[2];
		PairEndReads first = itr.next();
		PairEndReads second = null;
		PairEndReads per = null;
		while (!found && itr.hasNext()) {
			second = itr.next();
			if (second.isLeftReversed && first.isRightReversed) {
				pos[0] = (first.posLeft + second.posLeft) / 2;
				pos[1] = (first.posRight + second.posRight) / 2;
				found = true;
				while (itr.hasNext()) {
					per = itr.next();
					if (per.posLeft > pos[1]) {
						break;
					}
				}
			}
			first = second;
		}

		return pos;
	}

	public synchronized int find_min_pair_helper(PairEndReads p, Iterator<PairEndReads> itr) {
		// keep track of the temp min value
		int min = Integer.MAX_VALUE;
		int min_pos = 0;
		int diff = 0;
		boolean flag = false;
		do {
			diff = p.posRight - p.posLeft;
			if (0 < diff && diff < min) {
				min = diff;
				min_pos = (p.posLeft+p.posRight)/2;
				flag = true;
			} else {
				flag = false;
			}
		} while(flag && itr.hasNext() && (p = itr.next()) != null);

		return min_pos;
	}

	public void print_stat() {
		System.out.println();
		System.out.println();
		System.out.println("===============Result===============");
		System.out.println("Deteced total inversions: " + inversionPairs.size());
		System.out.println("Deteced inversions position (Diff is smaller better): ");
		int counter = 0;
		for (InversionPair ip : inversionPairs) {
			System.out.println(ip.posLeft + " - " + ip.posRight + ", accuracy: " + Math.abs(revList[counter] - ip.posLeft));
			counter++;
		}
	}

	public void debug() {
		// for testing
		for (PairEndReads p : pairList) {
			System.out.println("left: " + p.readLeft + " and right: "+p.readRight);
			// System.out.println("left pos: "+p.posLeft+" and right pos: "+p.posRight+" and diff: "+(p.posRight - p.posLeft));
			System.out.println("left pos: "+p.posLeft+" and right pos: "+p.posRight+" and isLeftReversed: "+p.isLeftReversed+" and isRightReversed: "+p.isRightReversed);
		}
	}
}