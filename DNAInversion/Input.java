package DNAInversion;

import java.util.*;
import java.io.*;

public class Input {
	// Here is the setting of the project
	// Most of the variance is defined here

	// public static int max = 1000000;
	// public static int max = 800000; 	// reduce a digit
	public static int max = 100000; 	// reduce 2 digit
	public static int debug_max = 2000;	// Debug genome length
	public static int revLen = 500;		// Reversed region length
	public static int interval = max;	// The interval of the max long length
	public static int readLength = 30;	// The length of read for one end pair
	public static int revCount = 3;		// Reversed region number
	public static int numOfModified = 2;// Errors
	public static String reference_file = "DNAInversion/reference_genome.txt";
	public static String human_file = "DNAInversion/human_genome.txt";
	public static String test_file = "DNAInversion/test_genome.txt";
	public static byte[] genome_byte = null;
	public static boolean debug = false;

	public Input() {}

	public static void generate_reference_genome() {
		// generate 1,000,000 string length with "ATCG"
		String dna = "ATCG";
		Random generator = new Random();
		StringBuffer buffer = new StringBuffer();
		int maxLen = max;
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(reference_file));
			while (maxLen > 0) {
				for (int i = 0; i < interval; i++) {
					int ranIndex = generator.nextInt(4); // generate 0-3
					buffer.append(dna.charAt(ranIndex));
				}
				bw.write(buffer.toString());
				// bw.newLine();
				buffer = buffer.delete(0,interval);
				maxLen -= interval;
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try	{
				if (bw != null) {
					bw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String get_file() {
		if (debug) {
			return test_file;
		} else {
			return human_file;
		}
	}

	public static String get_reference_file() {
		if (debug) {
			return test_file;
		} else {
			return reference_file;
		}
	}

	public static int get_max() {
		if (debug) {
			return debug_max;
		} else {
			return max;
		}
	}

	public static ArrayList<Integer> generate_human_genome() {
		// inverse few parts of the string from the reference genome
		BufferedReader br = null;
		BufferedWriter bw = null;
		Random generator = new Random();
		ArrayList<Integer> revList = new ArrayList<Integer>();
		ArrayList<Integer> addList = new ArrayList<Integer>();
		ArrayList<Integer> changeList = new ArrayList<Integer>();
		ArrayList<Integer> deleteList = new ArrayList<Integer>();
		int counter = 1;

		// try to reversed the genome and saved it
		try {
			String currentLine;
			br = new BufferedReader(new FileReader(reference_file));
			bw = new BufferedWriter(new FileWriter(human_file));
			while((currentLine = br.readLine()) != null) {
				StringBuffer sb = new StringBuffer(currentLine);

				// Apply reversing
				while(revCount > 0) {
					int revPos = get_random_pos();
					revList.add(revPos);
					int revPosEnd = revPos+revLen;
					StringBuffer revSb = reverse_string(sb, revPos, revPosEnd);
					sb = sb.replace(revPos, revPosEnd, revSb.toString());
					revCount--;
				}

				check_reversed(revList, sb);

				// Apply adding
				int numOfAdd = numOfModified;
				while(numOfAdd > 0) {
					int addPos = get_random_pos();
					char addGen = get_random_gene();
					addList.add(addPos);
					sb = sb.insert(addPos, addGen);
					numOfAdd--;
				}

				// Apply changing the gene
				int numOfchanged = numOfModified;
				while(numOfchanged > 0) {
					int changePos = get_random_pos();
					char changeGen = get_random_gene();
					changeList.add(changePos);
					sb.setCharAt(changePos, changeGen);
					numOfchanged--;
				}

				// Apply deleting
				int numOfDelete = numOfModified;
				while(numOfDelete > 0) {
					int deletePos = get_random_pos();
					deleteList.add(deletePos);
					sb = sb.deleteCharAt(deletePos);
					numOfDelete--;
				}

				counter++;
				bw.write(sb.toString());
				// bw.newLine();
			}
			bw.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				if (bw != null) {
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		System.out.println("Added: "+addList);
		System.out.println("Changed: "+changeList);
		System.out.println("Deleted: "+deleteList);

		return revList;
	}

	public static int get_random_pos() {
		// 5% chance to generate the inversion
		Random generator = new Random();
		// generate a reverse pos
		int revPos = generator.nextInt(interval - revLen);
		return revPos;
	}

	public static char get_random_gene() {
		Random generator = new Random();
		String dna = "ATCG";
		int pos = generator.nextInt(dna.length()); // generate 0 - 3 randomly
		return dna.charAt(pos);
	}

	public static StringBuffer reverse_string(StringBuffer origin, int start, int end) {
		String subStr = origin.substring(start, end);
		StringBuffer revSb = new StringBuffer(subStr);
		revSb = revSb.reverse();
		return revSb;
	}

	public static void check_reversed(ArrayList<Integer> positions, StringBuffer sb) throws DNAInversionException {
		int size = positions.size();
		for (int i = 0; i < size; i++) {
			String test1 = sb.substring(positions.get(i), Input.revLen + positions.get(i));
			String test2 = Input.get_input_stream(positions.get(i), Input.revLen, Input.get_reference_file());
			StringBuffer strbuf = new StringBuffer(test2);
			if (test1.equals(strbuf.reverse().toString())) {
				// System.out.println("Reversed");
			} else {
				System.out.println("Not reversed: " + positions.get(i));
				System.out.println(test1);
				System.out.println(strbuf.reverse());
				throw new DNAInversionException("Cannot create correct inversion.");
			}
		}
	}

	public static PairEndReads get_pair_end_reads() {
		Random generator = new Random();
		int gap = generate_pair_end_gap(generator);
		int revLenTmp = Math.max(gap, revLen);
		int posLeft = generator.nextInt(get_max() - readLength * 2 - revLenTmp);
		int posRight = posLeft+readLength+gap; // 500 is appoximate length between two reads

		String readLeft = get_input_stream(posLeft, readLength, get_file());
		String readRight = get_input_stream(posRight, readLength, get_file());

		return new PairEndReads(readLeft, posLeft, readRight, posRight);
	}

	public static int generate_pair_end_gap(Random generator) {
		// 70% of time will be 500
		int rand = generator.nextInt(10);
		if (rand <= 7) {
			return 500;
		} else {
			int sign = generator.nextInt(2);
			if (sign > 0) {
				return 500 + rand;
			} else {
				return 500 - rand;
			}
		}
	}

	// convert string into byte
	public static PairEndReads get_pair_end_reads(int startPos, int nextPos) {
		String[] reads = new String[2];
		byte[] read1 = Arrays.copyOfRange(genome_byte, startPos, startPos+readLength);
		byte[] read2 = Arrays.copyOfRange(genome_byte, nextPos, nextPos+readLength);
		try {
			reads[0] = new String(read1, "UTF-8");
			reads[1] = new String(read2, "UTF-8");
			PairEndReads pairReads = new PairEndReads(reads[0], startPos, reads[1], nextPos);
			return pairReads;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String get_input_stream(int pos, int length, String fileName) {
		BufferedReader br = null;
		char[] cbuf = new char[length];

		try {
			br = new BufferedReader(new FileReader(fileName));
			if (br.ready() && br.skip(pos) >= 0 && br.read(cbuf, 0, length) > 0) {
				return String.valueOf(cbuf);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public static int[] convertIntegers(ArrayList<Integer> integers)
	{
	    int[] ret = new int[integers.size()];
	    for (int i=0; i < ret.length; i++)
	    {
	        ret[i] = integers.get(i).intValue();
	    }
	    return ret;
	}

	public static String get_user_input_algorithm() {
		Console console = System.console();
		String input = console.readLine("Choose an algorithm \n 1: Naive sliding window \n 2: Indexing Hashtable \n 3: Indexing Hashtable with parallel :");
		return input;
	}

	public static int get_user_input_coverage() {
		Console console = System.console();
		String coverage = console.readLine("Coverage ?X : ");
		return Integer.valueOf(coverage);
	}
}