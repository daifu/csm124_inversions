package DNAInversion;

import java.util.*;

public class HumanGenome extends Genome {
	public int totalInersion;
	public ArrayList<Integer> inversionPos;

	public HumanGenome(String str) {
		super(str);
		totalInersion = 0;
		inversionPos = new ArrayList<Integer>();
	}
}