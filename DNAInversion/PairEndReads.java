package DNAInversion;

import java.util.*;

public class PairEndReads implements Comparable{
	public String 	readLeft;
	public String 	readRight;
	public int 		posLeft = -1;
	public int 		posRight = -1;
	public int 		readLength = Input.readLength; // by default, each reads has 100
	public boolean 	isLeftReversed = false;
	public boolean 	isRightReversed = false;

	public PairEndReads() {}

	public PairEndReads(String readLeft, int posLeft, String readRight, int posRight) {
		this.readLeft = readLeft;
		this.readRight = readRight;
		this.posLeft = posLeft;
		this.posRight = posRight;
	}

	public void set_read_length(int length) {
		this.readLength = length;
	}

	public String get_read_left() {
		return readLeft;
	}

	public String get_read_right() {
		return readRight;
	}

	public int get_gap() throws DNAInversionException {
		if (posLeft < 0 && posRight < 0) {
			this.isLeftReversed = false;
			this.isRightReversed = false;
			return 0;
			// throw new DNAInversionException("readLeft and readRight is not initialized");
		} else if (posLeft < 0 || posRight < 0) {
			this.isLeftReversed = false;
			this.isRightReversed = false;
			// return Integer.MAX_VALUE;
			return 0;
		}

		return Math.max(0, posRight - posLeft - readLength);
	}

	public int get_max_pos() {
		return Math.max(posLeft, posRight);
	}

	@Override
	public int compareTo(Object obj) {
		PairEndReads other = (PairEndReads) obj;
		// return this.get_max_pos() - other.get_max_pos();
		return this.posLeft - other.posLeft;
	}

	public void debug() {
		System.out.println("readLeft: " + readLeft);
		System.out.println("readLeft pos: " + posLeft);
		System.out.println("readRight: " + readRight);
		System.out.println("readRight pos: " + posRight);
	}
}