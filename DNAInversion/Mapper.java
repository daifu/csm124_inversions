package DNAInversion;
import java.util.*;
import java.io.*;

public class Mapper {
	public Hashtable<String, Integer> mapper;
	public static Hashtable<String, ArrayList<Integer>> hashtable = null;
	public static int key_length = 10;
	public static int num_parts = Input.readLength / key_length;
	public static int allow_mismatch = 1;
	public static int run_gap = 1;
	public Mapper() {
		mapper = new Hashtable<String, Integer>();
	}

	public void add(String snip, int pos) throws DNAInversionException {
		if (mapper.containsKey(snip)) {
			throw new DNAInversionException("DNA snip is duplicated: " + snip);
		}

		mapper.put(snip, pos);
	}

	public PairEndReads based_line_search(PairEndReads reads) {
		PairEndReads per = new PairEndReads();
		// read through the whole string file and search
		BufferedReader br = null;
		int start = 0;
		int end = Input.max - Input.readLength;
		boolean foundLeft = false;
		boolean foundRight = false;
		while ((!foundLeft || !foundRight) && (start <= end)) {
			String buf = Input.get_input_stream(start, Input.readLength, Input.get_reference_file());
			if (!foundLeft && equals(buf, reads.readLeft, per, 0)) {
				per.readLeft = new String(buf);
				per.posLeft = start;
				start += Input.readLength; // impove a speed a little bit
				foundLeft = true;
			}
			if (!foundRight && equals(buf, reads.readRight, per, 1)) {
				per.readRight = new String(buf);
				per.posRight = start;
				foundRight = true;
			}
			start++;
		}

		return per;
	}

	public synchronized PairEndReads hashtable_based_line_search(PairEndReads reads) throws DNAInversionException{
		PairEndReads per = new PairEndReads();
		// search left
		per.posLeft = ht_based_line_search(reads.readLeft);
		if (per.posLeft < 0) {
			per.posLeft = ht_based_line_search(reverse(reads.readLeft));
			if (per.posLeft > 0) {
				per.isLeftReversed = true;
			}
		}
		per.readLeft = reads.readLeft;

		// search right
		per.posRight = ht_based_line_search(reads.readRight);
		if (per.posRight < 0) {
			per.posRight = ht_based_line_search(reverse(reads.readRight));
			if (per.posRight > 0) {
				per.isRightReversed = true;
			}
		}
		per.readRight = reads.readRight;


		return per;
	}

	private int ht_based_line_search(String str) {
		// divide the string into three part
		ArrayList<Integer> posList = new ArrayList<Integer>();
		int size = Input.readLength - key_length;
		for (int i = 0; i < size; i++) {
			String substr = str.substring(i, i + key_length);
			// exactly match
			if (hashtable.containsKey(substr)) {
				posList.addAll(hashtable.get(substr));
			}
		}
		if (posList.size() > 0) {
			int[] posListInt = Input.convertIntegers(posList);
			Arrays.sort(posListInt);
			return get_indexOf_mismatch(posListInt);
			// return get_indexOf(posListInt);
		} else {
			return -1;
		}
	}

	private int get_indexOf(int[] sorted_ary) {
		int size = sorted_ary.length;
		for (int i = 0; i < size; i++) {
			int index1 = Arrays.binarySearch(sorted_ary, i+1, size, sorted_ary[i]+key_length);
			if(index1 >= 0) {
				// here will be make sure that it found the index
				int index2 = Arrays.binarySearch(sorted_ary, index1+1, size, sorted_ary[index1]+key_length);
				if (index2 >= 0) {
					return sorted_ary[i];
				}
			}
		}
		return -1;
	}

	private int get_indexOf_mismatch(int[] sorted_ary) {
		int size = sorted_ary.length - 1;
		int allow_min_match_length = (Input.readLength - key_length) / run_gap - 2;
		int allow_max_gap = run_gap * Input.readLength + run_gap;
		int first = 0;
		int total_matches = 1;
		for (int i = 0; i < size; i++) {
			// allow_mismatch = 1 or 2;
			int diff = sorted_ary[i+1] - sorted_ary[i];
			if (0 <= diff && diff <= allow_max_gap) {
				total_matches++;
				if (allow_min_match_length <= total_matches) {
					return sorted_ary[first];
				}
			} else {
				total_matches = 1;
				first = i+1;
			}
		}
		return -1;
	}

	public void build_hash_table() {
		// L = 30, each entry will have a key of length 10. On average it will have 1000000/4^10 positions
		// int max_keys = (int)Math.pow(4, key_length);
		hashtable = new Hashtable<String, ArrayList<Integer>>();
		int start = 0;
		int end = Input.get_max() - key_length;

		do {
			String key = Input.get_input_stream(start, key_length, Input.get_reference_file());
			if (hashtable.containsKey(key)) {
				ArrayList<Integer> tmp = hashtable.get(key);
				tmp.add(start);
				hashtable.put(key, tmp);
			} else {
				ArrayList<Integer> tmp = new ArrayList<Integer>();
				tmp.add(start);
				hashtable.put(key, tmp);
			}
			start+=run_gap;
		} while (start <= end);
		return;
	}

	public void parallel_build_hash_table(int num_of_threads) {
		hashtable = new Hashtable<String, ArrayList<Integer>>();
		Thread[] threads = new Thread[num_of_threads];
		int thread_range = Input.get_max() / num_of_threads;

		for (int i = 0; i < num_of_threads; i++) {
			int start = i * thread_range;
			int end = start + thread_range;
			threads[i] = new BuildThreads(hashtable, start, end, run_gap);
		}

		for (int i = 0; i < num_of_threads; i++) {
			threads[i].start();
		}

		try {
			for (int i = 0; i < num_of_threads; i++) {
				threads[i].join();
			}
		} catch (InterruptedException e) {
			System.out.println(e);
		}

		return;
	}

	public boolean equals(String buf, String str, PairEndReads per, int direction) {
		String rev_str = reverse(str);
		if (mismatch_equals(buf, str, allow_mismatch)) {
			return true;
		} else if (mismatch_equals(buf, rev_str, allow_mismatch)) {
			if (direction == 0) {
				per.isLeftReversed = true;
			} else {
				per.isRightReversed = true;
			}
			return true;
		}
		return false;
	}

	public String reverse(String origin) {
		StringBuffer tmp = new StringBuffer(origin);
		tmp = tmp.reverse();
		return tmp.toString();
	}

	public boolean mismatch_equals(String buf, String str, int allow_mismatch) {
		int mismatch_count = 0;
		for (int i = 0; i < buf.length(); i++) {
			if (buf.charAt(i) != str.charAt(i)) {
				mismatch_count++;
			}
			if (mismatch_count > allow_mismatch) {
				return false;
			}
		}
		return true;
	}

	public void map_reference_genome() {
		BufferedReader br = null;
		int counter = 0;
		String curLine;
		String passover = "";
		StringBuffer buf = new StringBuffer();

		try	{
			br = new BufferedReader(new FileReader(Input.reference_file));
			// br = new BufferedReader(new FileReader(Input.test_file));
			while ((curLine = br.readLine()) != null) {
				buf.delete(0,buf.length());
				buf.append(passover);
				buf.append(curLine);
				int size = buf.length() - Input.readLength;
				int length = Input.readLength;
				int i = 0;
				do {
					if (i+length == buf.length()) {
						passover = buf.substring(i+1,i+length);
					}
					String substr = buf.substring(i, i+length);
					add(substr, counter*Input.interval+i);
					i++;
				} while (i <= size);
				counter++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public void debug() {
		// debug mapper
		Enumeration<String> enumKey = hashtable.keys();
		while (enumKey.hasMoreElements()) {
			String key = enumKey.nextElement();
			ArrayList<Integer> pos = hashtable.get(key);
			System.out.println(key + " : "+pos);
		}
		System.out.println(hashtable.size());
	}
}