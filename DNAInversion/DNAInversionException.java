package DNAInversion;

@SuppressWarnings("serial")
public class DNAInversionException extends RuntimeException {

	public DNAInversionException() {
		super();
	}

	public DNAInversionException(String s) {
		super(s);
	}
}