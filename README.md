Required Java

How to run:

1. go into the project folder

2. type `make compile`

3. type `make run`
	1. input a coverage (coverage is explained below)
	2. select an algorithm to run (eg. 3)

4. Done, and display the stats of the run

Input:

1. Generate about 1,000,000 random string with characters of ATCG

2. Human genome: Modify the reference genome to create human genome.
	1. Insertation (Location, Symbol)
	2. Deletion (Location, Symbol)
	3. Modification (Location, From_Symbol, To_Symbol)
	4. Inversion (Location, length)

3. Paired End Reads: Extract it from the human genome. Its format follows:
	1. Length: read1(100) + normal distribution of (mean=500, sd=10) + read2(100)

Output:

1. A lot of End Reads that have large gap in sorted array based on the mapped position.
2. Return reversed direction pairs of Paired End Reads with large gap.
3. Indicated inversions by inversions signature from step 2.

Mapper for reference genome:

Hash map (KEY: string (length of 100), VALUE: linked_list of all the positions)

Coverage = 2L * N / Lenght of genome = 2 * 100 * N / 1,000,000.
Example: Then if N = 50000, it has 10X coverage, if N = 75000, it has 15X coverage, if N = 100000, it has 20X coverage ...
